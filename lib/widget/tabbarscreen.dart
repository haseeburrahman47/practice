import 'package:flutter/material.dart';
import 'package:tomisha_task/constant/color.dart';

class TabBarScreen extends StatefulWidget {
  String name1,
      name2,
      name3,
      name4,
      name5,
      name6,
      name7,
      name8,
      name9,
      image1,
      image2,
      image3;

  TabBarScreen(
      {required this.name1,
      required this.name2,
      required this.name3,
      this.name4 = '',
      required this.name5,
      this.name6 = '',
      required this.name7,
      required this.name8,
      this.name9 = '',
      required this.image1,
      required this.image2,
      required this.image3});

  @override
  State<TabBarScreen> createState() => _TabBarScreenState();
}

class _TabBarScreenState extends State<TabBarScreen> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 30),
        Align(
          alignment: Alignment.center,
          child: Text(
            widget.name1,
            style: TextStyle(
              fontFamily: 'Lado',
              fontSize: 25.0,
            ),
          ),
        ),
        Align(
          alignment: Alignment.center,
          child: Text(
            widget.name2,
            style: TextStyle(
              fontFamily: 'Lado',
              fontSize: 25.0,
            ),
          ),
        ),
        SizedBox(height: 30),
        Container(
          height: 200,
          width: MediaQuery.of(context).size.width - 40,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(widget.image1), fit: BoxFit.fitHeight)),
        ),
        SizedBox(height: 30),
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              '1',
              style: TextStyle(color: text_color, fontSize: 80),
            ),
            Text(
              '.',
              style: TextStyle(color: text_color, fontSize: 80),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.name3,
                  style: TextStyle(color: text_color, fontSize: 20),
                ),
                Text(
                  widget.name4,
                  style: TextStyle(color: text_color, fontSize: 20),
                ),
              ],
            ),
          ],
        ),
        SizedBox(height: 30),
        ClipPath(
          clipper: WaveCliper(),
          child: Container(
            height: 400,
            color: gradient_color2,
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      '2',
                      style: TextStyle(color: text_color, fontSize: 80),
                    ),
                    Text(
                      '.',
                      style: TextStyle(color: text_color, fontSize: 80),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.name5,
                          style: TextStyle(color: text_color, fontSize: 20),
                        ),
                        Text(
                          widget.name6,
                          style: TextStyle(color: text_color, fontSize: 20),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Container(
                  height: 200,
                  width: MediaQuery.of(context).size.width - 40,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(widget.image2),
                          fit: BoxFit.fitHeight)),
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 30),
        CircleAvatar(
          radius: 150,
          backgroundColor: circle_shape_color,
          child: Stack(
            clipBehavior: Clip.none,
            children: [
              Positioned(
                top: 0.0,
                left: 30,
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          '3',
                          style: TextStyle(color: text_color, fontSize: 80),
                        ),
                        Text(
                          '.',
                          style: TextStyle(color: text_color, fontSize: 80),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.name7,
                              style: TextStyle(color: text_color, fontSize: 20),
                            ),
                            Text(
                              widget.name8,
                              style: TextStyle(color: text_color, fontSize: 20),
                            ),
                            Text(
                              widget.name9,
                              style: TextStyle(color: text_color, fontSize: 20),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Positioned(
                top: 120,
                left: 30,
                child: Container(
                  height: 200,
                  width: MediaQuery.of(context).size.width - 40,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(widget.image3),
                          fit: BoxFit.fitHeight)),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}

class WaveCliper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    debugPrint(size.width.toString());
    var path = Path();
    path.lineTo(0, size.height);
    var firstStart = Offset(size.width / 5, size.height);
    var firstEnd = Offset(size.width / 2.25, size.height - 50);

    path.quadraticBezierTo(
        firstStart.dx, firstStart.dy, firstEnd.dx, firstEnd.dy);

    var secondStart =
        Offset(size.width - (size.width / 3.24), size.height - 105);

    var secondEnd = Offset(size.width, size.height - 10);

    path.quadraticBezierTo(
        secondStart.dx, secondStart.dy, secondEnd.dx, secondEnd.dy);

    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    throw UnimplementedError();
  }
}
