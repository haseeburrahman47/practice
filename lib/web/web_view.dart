import 'package:flutter/material.dart';
import 'package:tomisha_task/constant/color.dart';
import 'package:tomisha_task/tab_screen/arbeitgeber_screen.dart';
import 'package:tomisha_task/tab_screen/arbeitnehmer_screen.dart';
import 'package:tomisha_task/tab_screen/temporarburo_screen.dart';

class DesktopViewScreen extends StatefulWidget {
  const DesktopViewScreen({Key? key}) : super(key: key);

  @override
  State<DesktopViewScreen> createState() => _DesktopViewScreenState();
}

class _DesktopViewScreenState extends State<DesktopViewScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
          body: SafeArea(
              child: Scaffold(
                  body: ListView(shrinkWrap: true, children: [
        Container(
            height: 400,
            decoration: BoxDecoration(
                gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                gradient_color1,
                gradient_color2,
              ],
            )),
            child: Column(children: [
              Container(
                height: 80,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.2),
                        spreadRadius: 2.0,
                        blurRadius: 4.0,
                      )
                    ],
                    color: Colors.white,
                    borderRadius: const BorderRadius.only(
                        bottomLeft: Radius.circular(15.0),
                        bottomRight: Radius.circular(15.0))),
                child: Stack(
                  children: [
                    Positioned(
                        top: 0.0,
                        child: Container(
                          height: 5,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            colors: [
                              blue_color,
                              teal_color,
                            ],
                          )),
                        )),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: Padding(
                        padding: const EdgeInsets.only(right: 20.0, bottom: 30),
                        child: Text(
                          'Login',
                          style: TextStyle(
                              color: teal_color,
                              fontFamily: 'Lato',
                              fontSize: 18),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 15.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      SizedBox(height: 50),
                      Align(
                        alignment: Alignment.topCenter,
                        child: Text(
                          'Deine Job',
                          style: TextStyle(
                              fontFamily: 'Lado',
                              fontSize: 40.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Align(
                        alignment: Alignment.topCenter,
                        child: Text(
                          'website',
                          style: TextStyle(
                            fontFamily: 'Lado',
                            fontWeight: FontWeight.bold,
                            fontSize: 40.0,
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      Center(
                        child: Container(
                          height: 50,
                          width: 170,
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey.withOpacity(0.2),
                                    blurRadius: 4.0,
                                    spreadRadius: 2.0)
                              ],
                              borderRadius: BorderRadius.circular(20),
                              gradient: LinearGradient(
                                begin: Alignment.topRight,
                                end: Alignment.bottomLeft,
                                colors: [
                                  blue_color,
                                  teal_color,
                                ],
                              )),
                          child: const Center(
                            child: const Text(
                              'Kostenlos Registrieren',
                              style: TextStyle(
                                  color: Colors.white, fontFamily: 'Lado'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    width: 30,
                    height: 20,
                  ),
                  CircleAvatar(
                    backgroundColor: Colors.white,
                    radius: 100,
                    child: Image.asset(
                      'assets/images/hands.png',
                      fit: BoxFit.cover,
                    ),
                  )
                ],
              ),
            ])),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: 60,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      border: Border.all(
                          color: teal_color,
                          width: 0.5,
                          style: BorderStyle.solid)),
                  child: TabBar(
                      isScrollable: true,
                      unselectedLabelColor: teal_color,
                      indicator: BoxDecoration(
                        color: tabBar_color,
                        borderRadius: BorderRadius.circular(30),
                      ),
                      tabs: const [
                        Tab(
                          text: 'Arbeitnehmer',
                        ),
                        Tab(text: 'Arbeitgeber'),
                        Tab(text: 'Temporärbüro'),
                      ]),
                ),
                SizedBox(
                    height: 1300,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 10.0,
                      ),
                      child: TabBarView(
                        children: [
                          ArbeitnehmerScreen(),
                          ArbeitgeberScreen(),
                          TemporaburoScreen(),
                        ],
                      ),
                    )),
              ],
            ),
          ),
        ),
      ])))),
    );
  }
}
