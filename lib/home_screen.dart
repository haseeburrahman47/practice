import 'package:flutter/material.dart';
import 'package:tomisha_task/constant/custom_button.dart';
import 'package:tomisha_task/mobile_view_screen.dart';
import 'package:tomisha_task/web/web_view.dart';

import 'constant/color.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(left: 20, right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 40),
              Text("yeah you rocks👏, now you're in the next level.",
                  style: text_style),
              SizedBox(height: 10.0),
              Text(
                "Now you can create the test page and show how skilled you are. ",
                style: text_style,
              ),
              Text(
                "- Only frontend ",
                style: text_style,
              ),
              Text("- With Flutter", style: text_style),
              Text("- Full responsive webview", style: text_style),
              SizedBox(height: 10),
              Text(
                "Create this test page and push it to GitLab.",
                style: text_style,
              ),
              SizedBox(height: 10),
              Text(
                  "Send me via Slack a message when you will start with the test "
                  "page and when you will be finished.",
                  style: text_style),
              SizedBox(
                height: 15.0,
              ),
              CustomButton(
                  ontap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MobileViewScreen()));
                  },
                  text_name: 'Mobile View'),
              SizedBox(
                height: 15.0,
              ),
              CustomButton(
                  ontap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DesktopViewScreen()));
                  },
                  text_name: 'Desktop View'),
              SizedBox(
                height: 15.0,
              ),
              CustomButton(ontap: () {}, text_name: 'Desktop scroll'),
              SizedBox(height: 10),
              Text(
                  """The website can be scrolled, while the button"""
                  """ "Kostenlos Registrieren" goes on the top bar.""",
                  style: text_style),
            ],
          ),
        ),
      ),
    );
  }
}
