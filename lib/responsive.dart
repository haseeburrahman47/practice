import 'package:flutter/material.dart';

class Responsive extends StatelessWidget {
  final Widget mobile, tablet, desktop;
  Responsive(
      {required this.mobile, required this.desktop, required this.tablet});

  static bool isMobile(BuildContext context) =>
      MediaQuery.of(context).size.width < 650;

  static bool isTablet(BuildContext context) =>
      MediaQuery.of(context).size.width < 1100 &&
      MediaQuery.of(context).size.width < 650;

  static bool isDesktop(BuildContext context) =>
      MediaQuery.of(context).size.width < 1100;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      /// if our width is more than 1100 then we consider it a desktop
      if (constraints.maxWidth >= 1100) {
        return desktop;
      }

      /// if our width is more than 650 then we consider it a mobile
      else if (constraints.maxWidth >= 650) {
        return mobile;
      }

      /// if our width is more than 650 then we consider it a tablet
      else {
        return tablet;
      }
    });
  }
}
