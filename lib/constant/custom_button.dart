import 'package:flutter/material.dart';
import 'package:tomisha_task/constant/color.dart';

class CustomButton extends StatelessWidget {
  String text_name;
  Function ontap;

  CustomButton({required this.text_name, required this.ontap});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      width: MediaQuery.of(context).size.width - 40,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          border: Border.all(
            width: 1,
            color: button_border_color,
            style: BorderStyle.solid,
          )),
      child: MaterialButton(
        onPressed: () {
          ontap();
        },
        child: Center(
          child: Text(
            text_name,
            style:
                TextStyle(color: teal_color, fontSize: 15, fontFamily: 'Lato'),
          ),
        ),
      ),
    );
  }
}
