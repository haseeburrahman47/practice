import 'package:flutter/material.dart';

final teal_color = Color(0xFF319795);
final background_color = Color(0xFF1E1C44);
final text_color = Color(0xFF4A5568);
final button_border_color = Color(0xFFCBD5E0);
final blue_color = Color(0xFF3182CE);
final gradient_color1 = Color(0xFFEBF4FF);
final gradient_color2 = Color(0xFFE6FFFA);
final tabBar_color = Color(0xFF81E6D9);
final circle_shape_color = Color(0xFFF7FAFC);
final text_style = TextStyle(color: text_color, fontSize: 23);
