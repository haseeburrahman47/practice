import 'dart:async';

import 'package:flutter/material.dart';
import 'package:tomisha_task/constant/color.dart';
import 'package:tomisha_task/home_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 5),
        () => Navigator.push(
            context, MaterialPageRoute(builder: (context) => HomeScreen())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: background_color,
      body: Padding(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 70.0),
            Align(
              alignment: Alignment.topRight,
              child: Image.asset(
                'assets/images/rocket.png',
                height: 250,
                width: 250,
              ),
            ),
            Text(
              'Flutter',
              style: TextStyle(
                color: teal_color,
                fontSize: 80.0,
              ),
            ),
            SizedBox(height: 10),
            Text(
              'Test',
              style: TextStyle(
                color: teal_color,
                fontSize: 80.0,
              ),
            ),
            SizedBox(height: 10),
            Text(
              'Index',
              style: TextStyle(
                color: teal_color,
                fontSize: 80.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
