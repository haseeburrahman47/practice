import 'package:flutter/material.dart';
import 'package:tomisha_task/constant/color.dart';
import 'package:tomisha_task/responsive.dart';
import 'package:tomisha_task/tab_screen/arbeitgeber_screen.dart';
import 'package:tomisha_task/tab_screen/arbeitnehmer_screen.dart';
import 'package:tomisha_task/tab_screen/temporarburo_screen.dart';

class MobileViewScreen extends StatefulWidget {
  const MobileViewScreen({Key? key}) : super(key: key);

  @override
  State<MobileViewScreen> createState() => _MobileViewScreenState();
}

class _MobileViewScreenState extends State<MobileViewScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: SafeArea(
        child: Scaffold(
          body: ListView(
            shrinkWrap: true,
            children: [
              Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [
                    gradient_color1,
                    gradient_color2,
                  ],
                )),
                child: Column(
                  children: [
                    Container(
                      height: 80,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.2),
                              spreadRadius: 2.0,
                              blurRadius: 4.0,
                            )
                          ],
                          color: Colors.white,
                          borderRadius: const BorderRadius.only(
                              bottomLeft: Radius.circular(15.0),
                              bottomRight: Radius.circular(15.0))),
                      child: Stack(
                        children: [
                          Positioned(
                              top: 0.0,
                              child: Container(
                                height: 5,
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                  begin: Alignment.topRight,
                                  end: Alignment.bottomLeft,
                                  colors: [
                                    blue_color,
                                    teal_color,
                                  ],
                                )),
                              )),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  right: 20.0, bottom: 30),
                              child: Text(
                                'Login',
                                style: TextStyle(
                                    color: teal_color,
                                    fontFamily: 'Lato',
                                    fontSize: 18),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 15.0),
                    Align(
                      alignment: Alignment.center,
                      child: Text(
                        'Deine Job',
                        style: TextStyle(
                          fontFamily: 'Lado',
                          fontSize: 40.0,
                        ),
                      ),
                    ),
                    const Align(
                      alignment: Alignment.center,
                      child: Text(
                        'website',
                        style: TextStyle(
                          fontFamily: 'Lado',
                          fontSize: 40.0,
                        ),
                      ),
                    ),
                    Container(
                      height: 300,
                      width: MediaQuery.of(context).size.width,
                      decoration: const BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                'assets/images/hands.png',
                              ),
                              fit: BoxFit.fill)),
                    ),
                    const SizedBox(height: 20),
                    Container(
                      height: 1600,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                blurRadius: 4.0,
                                spreadRadius: 2.0)
                          ],
                          color: Colors.white,
                          borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20))),
                      child: Column(
                        children: [
                          const SizedBox(height: 40),
                          Container(
                            height: 50,
                            width: MediaQuery.of(context).size.width - 40,
                            decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(0.2),
                                      blurRadius: 4.0,
                                      spreadRadius: 2.0)
                                ],
                                borderRadius: BorderRadius.circular(20),
                                gradient: LinearGradient(
                                  begin: Alignment.topRight,
                                  end: Alignment.bottomLeft,
                                  colors: [
                                    blue_color,
                                    teal_color,
                                  ],
                                )),
                            child: const Center(
                              child: const Text(
                                'Kostenlos Registrieren',
                                style: TextStyle(
                                    color: Colors.white, fontFamily: 'Lado'),
                              ),
                            ),
                          ),
                          const SizedBox(height: 40),
                          const Divider(),
                          const SizedBox(height: 25.0),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20.0, vertical: 20),
                            child: SingleChildScrollView(
                              child: Column(
                                children: [
                                  Container(
                                    height: 60,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(30),
                                        border: Border.all(
                                            color: teal_color,
                                            width: 0.5,
                                            style: BorderStyle.solid)),
                                    child: TabBar(
                                        isScrollable: true,
                                        unselectedLabelColor: teal_color,
                                        indicator: BoxDecoration(
                                          color: tabBar_color,
                                          borderRadius:
                                              BorderRadius.circular(30),
                                        ),
                                        tabs: const [
                                          Tab(
                                            text: 'Arbeitnehmer',
                                          ),
                                          Tab(text: 'Arbeitgeber'),
                                          Tab(text: 'Temporärbüro'),
                                        ]),
                                  ),
                                  SizedBox(
                                      height: 1300,
                                      child: const TabBarView(
                                        children: [
                                          ArbeitnehmerScreen(),
                                          ArbeitgeberScreen(),
                                          TemporaburoScreen(),
                                        ],
                                      )),
                                ],
                              ),
                            ),
                          ),
                          //
                        ],
                      ),
                    ),
                    const SizedBox(height: 25.0),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
