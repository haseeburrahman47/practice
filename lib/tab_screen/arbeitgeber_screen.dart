import 'package:flutter/material.dart';
import 'package:tomisha_task/constant/color.dart';
import 'package:tomisha_task/tab_screen/arbeitnehmer_screen.dart';
import 'package:tomisha_task/widget/tabbarscreen.dart';

class ArbeitgeberScreen extends StatefulWidget {
  const ArbeitgeberScreen({Key? key}) : super(key: key);

  @override
  State<ArbeitgeberScreen> createState() => _ArbeitgeberScreenState();
}

class _ArbeitgeberScreenState extends State<ArbeitgeberScreen> {
  @override
  Widget build(BuildContext context) {
    return TabBarScreen(
        name1: 'Drei einfache Schritte ',
        name2: 'zu deinem neuen Mitarbeiter',
        name3: 'Erstellen dein Unternehmensprofil',
        name4: '',
        name5: 'Erstellen ein Jobinserat ',
        name6: '',
        name7: 'Wahle deinen',
        name8: 'neuen Mitarbeiter aus',
        name9: '',
        image1: 'assets/images/erstellen.png',
        image2: 'assets/images/joben.png',
        image3: 'assets/images/wahle.png');
  }
}
