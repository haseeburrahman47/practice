import 'package:flutter/material.dart';
import 'package:tomisha_task/widget/tabbarscreen.dart';

class ArbeitnehmerScreen extends StatefulWidget {
  const ArbeitnehmerScreen({Key? key}) : super(key: key);

  @override
  State<ArbeitnehmerScreen> createState() => _ArbeitnehmerScreenState();
}

class _ArbeitnehmerScreenState extends State<ArbeitnehmerScreen> {
  @override
  Widget build(BuildContext context) {
    return TabBarScreen(
        name1: 'Drei einfache Schritte',
        name2: 'zu deinem neuen Job',
        name3: 'Erstellen dein Lebenslauf',
        name4: '',
        name5: 'Erstellen dein Lebenslauf',
        name6: '',
        name7: 'Mit nur einem Klick',
        name8: 'bewerben',
        name9: '',
        image1: 'assets/images/erstellen.png',
        image2: 'assets/images/erstellen_man.png',
        image3: 'assets/images/mit_nur.png');
  }
}
