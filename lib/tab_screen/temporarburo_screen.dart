import 'package:flutter/material.dart';
import 'package:tomisha_task/widget/tabbarscreen.dart';

class TemporaburoScreen extends StatefulWidget {
  const TemporaburoScreen({Key? key}) : super(key: key);

  @override
  State<TemporaburoScreen> createState() => _TemporaburoScreenState();
}

class _TemporaburoScreenState extends State<TemporaburoScreen> {
  @override
  Widget build(BuildContext context) {
    return TabBarScreen(
        name1: 'Drei einfache Schritte',
        name2: 'zur Vermittlung neuer Mitarbeiter',
        name3: 'Erstellen dein ',
        name4: 'Unternehmensprofil',
        name5: 'Erhalte Vermittings-',
        name6: 'angetbot von Arbeitgeber',
        name7: 'Vermttinlung nach',
        name8: 'Provision oder',
        name9: 'Stundenlohn',
        image1: 'assets/images/erstellen.png',
        image2: 'assets/images/angebot.png',
        image3: 'assets/images/vermitting.png');
  }
}
